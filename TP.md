# TP - Mise en place d'un serveur web Apache + PHP sous Docker

Ce TP à pour but de vous apprendre à mettre en place un serveur web Apache + PHP.

---

## Recherche d'une image PHP sur le hub

Docker met à disposition un grand nombre d'images déjà pré-configurée par la communauté disponibles sur le hub docker.

[HUB.DOCKER.COM](https://hub.docker.com)

Cherchez donc si une image PHP avec Apache existe.

[PHP DOCKER HUB](https://hub.docker.com/_/php/)

Utilisez le tag `7.0.27-apache-jessie`.

Ce tag signifi que l'image utilisé sera une `PHP 7.0.27` avec `Apache` dans une distribution `Linux Debian Jessie`.

## Lancement de l'image avec un tag

Executons notre premier conteneur PHP contenant Apache aussi.

Dans la section `With Apache` => `Without a Dockerfile`, les developpeurs expliquent comment lancer leur conteneur.

```
$ docker run -p 80:80 --name my-apache-php-app -v "$PWD":/var/www/html php:7.0.27-apache-jessie
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.3. Set the 'ServerName' directive globally to suppress this message
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.3. Set the 'ServerName' directive globally to suppress this message
[Wed Feb 21 21:57:18.648476 2018] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.10 (Debian) PHP/7.0.27 configured -- resuming normal operations
[Wed Feb 21 21:57:18.648547 2018] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
```

Commande `docker run -p 80:80 --name my-apache-php-app -v "$PWD":/var/www/html php:7.0.27-apache-jessie`

docker run
* `-p 80:80`: expose le port 80 de votre machine avec le port 80 interne du conteneur
* `--name my-apache-php-app`: donne un nom au conteneur, ici `my-apache-php-app`
* `-v "$PWD":/var/www/html`: mappage du dossier courant avec le dossier `/var/www/html` du conteneur
* `php:7.0.27-apache-jessie`: utilisation de l'image `php` avec le tag `7.0.27-apache-jessie`

> Dans le repo git ce trouve un fichier index.php avec un hello world et un phpinfo() pour voir le bon fonctionnement du conteneur.

## Test du bon fonctionnement du conteneur

Testez maintenant que votre service est fonctionnel.

Allez sur l'url : [http://localhost](http://localhost)

Cette page devrait apparaitre:

![LOCALHOST](localhost.jpg)

Vous pouvez voir également dans votre terminal qu'une ligne c'est affiché:
```shell
172.17.0.1 - - [21/Feb/2018:21:59:15 +0000] "GET / HTTP/1.1" 200 23553 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
```

Ce sont les logs du conteneur qui s'affiche en direct sur la sortie standart de votre terminal.

Faites un `Ctrl + C` pour stopper le contenur.

```shell
^C[Wed Feb 21 22:03:24.199831 2018] [mpm_prefork:notice] [pid 1] AH00169: caught SIGTERM, shutting down
```

Supprimez le conteneur maintenant.

## Lancement en mode daemon

Le PHP conteneur étant lancé sans l'option daemon, l'affichage des logs se faisait sur la sortie standart de votre terminal, pas pratique pour continuer des actions ensuite.

Pour cela, `docker run` possede une option `-d` qui permet de lancer les conteneurs en mode daemon.

Refaite la commande precedente avec l'option `-d` pour lancer le conteneur `my-apache-php-app` en mode daemon.

La sortie du terminal devrait être semblable à un UID du type :  `5fc5b415115346a80afbd36a90b6ef7be132435e895d093c8676b53d6dc89060`

## Verification

Verifiez maintenant que votre conteneur fonctionne correctement en retournant sur l'url [http://localhost](http://localhost) et en faisant un `docker ps`.

```shell
$ docker ps
CONTAINER ID        IMAGE                      COMMAND                  CREATED              STATUS              PORTS                NAMES
5fc5b4151153        php:7.0.27-apache-jessie   "docker-php-entrypoi…"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp   my-apache-php-app
```

Vous avez maintenant un conteneur PHP qui tourne sur votre machine sans même n'avoir rien eu à configurer.

Pour debugger, certains moment vous allez avoir besoin de voir les logs.

Utilisez l'option `docker logs UID|CONTENEURNAME`

```shell
$ docker logs my-apache-php-app
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.3. Set the 'ServerName' directive globally to suppress this message
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.3. Set the 'ServerName' directive globally to suppress this message
[Wed Feb 21 22:06:45.499337 2018] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.10 (Debian) PHP/7.0.27 configured -- resuming normal operations
[Wed Feb 21 22:06:45.499450 2018] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
```

Utilisez l'option `-f` pour avoir un affichage des logs en continue.
