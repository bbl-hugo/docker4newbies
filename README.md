# BBL - DOCKER 4 NEWBIES

Git repository for docker4newbies bbl.

---

## Usefull commands

### Test Docker version
Ensure that you have a supported version of Docker:

```
$ docker --version
Docker version 17.12.0-ce, build c97c6d6
```

### Test docker installation

Test that your installation works by running the simple Docker image, [hello-world](https://hub.docker.com/_/hello-world/) :
```shell
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
ca4f61b1923c: Pull complete
Digest: sha256:083de497cff944f969d8499ab94f07134c50bcf5e6b9559b27182d3fa80ce3f7
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

...
```

### List docker images

List the ```hello-world``` image that was downloaded to your machine:
```shell
$ docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
hello-world         latest              f2a91732366c        2 months ago        1.85kB
```

### Show running container

List of the running container:
```shell
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

Show full list ( like container stopped ):
```shell
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                  PORTS               NAMES
4f1c4ab87255        hello-world         "/hello"            2 days ago          Exited (0) 2 days ago                       agitated_goldstine
```

You can see the container ```hello-world```.

### Remove container

Now you have launch your first container, you need to remove them to clean your docker install.

```shell
$ docker rm 4f1c4ab87255
4f1c4ab87255
```

You can check with command ```docker ps -a``` if the container is again here.

```shell
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

No more container available.

### Launch container as daemon

#### Use tutum/hello-world

If you container need to be accessible as a daemon, you can run with option ```-d```:

```shell
$ docker run -d -p 8080:80 tutum/hello-world
Unable to find image 'tutum/hello-world:latest' locally
latest: Pulling from tutum/hello-world
658bc4dc7069: Pull complete
a3ed95caeb02: Pull complete
af3cc4b92fa1: Pull complete
d0034177ece9: Pull complete
983d35417974: Pull complete
Digest: sha256:0d57def8055178aafb4c7669cbc25ec17f0acdab97cc587f30150802da8f8d85
Status: Downloaded newer image for tutum/hello-world:latest
543d933d26e5425b9e66aa89f917cb70eae002e8d8d0ca426946b2c9d9a54d84
```

This command execute the image ```tutum/hello-world``` with ```-d``` for running in daemon, ```-p 8080:80``` it's for mapping the port 8080 in localhost into the container on port 80.

The last line is the UID of the container : ```543d933d26e5425b9e66aa89f917cb70eae002e8d8d0ca426946b2c9d9a54d84```

#### Test it

Now you can try to connect to ```http://localhost:8080```

```html
$ curl http://localhost:8080
1  <html>
2  <head>
3	 <title>Hello world!</title>
4	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
5	 <style>
6	 body {
7		 background-color: white;
8		 text-align: center;
9		 padding: 50px;
10		 font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
11	 }
12
13	 #logo {
14		 margin-bottom: 40px;
15	 }
16	 </style>
17  </head>
18  <body>
19  	<img id="logo" src="logo.png" />
20  	<h1>Hello world!</h1>
21  	<h3>My hostname is 543d933d26e5</h3>	</body>
22  </html>
```

To check if it's your container, check the line 21 below ```Hello world!```, the container echo the first char of UID ```My hostname is 543d933d26e5```.

To check if all is ok inside this the command ```docker logs UID```.
```shell
$ docker logs 543d933d26e5
```

#### !! docker stop & docker rm !!

To stop it, you can use :
* ```docker stop UID``` : use this command if you want reused it later, the container is just stop and can be restart with ```docker start UID```.
* ```docker rm -f UID``` : use this command if you want stopped and removed the container.





Credit :

![Symbol-IT](https://symbol-it.fr/img/fotolia/logo_symbol-it-texte-Bleu.png)
